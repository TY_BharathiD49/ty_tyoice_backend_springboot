package com.tyss.tyoice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TyoiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TyoiceApplication.class, args);
	}

}
