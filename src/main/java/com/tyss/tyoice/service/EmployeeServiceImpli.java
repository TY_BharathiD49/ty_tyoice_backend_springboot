package com.tyss.tyoice.service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tyss.tyoice.dao.EmployeeDao;
import com.tyss.tyoice.dto.ClientProject;
import com.tyss.tyoice.dto.Employee;

@Service
public class EmployeeServiceImpli implements EmployeeService{

	
	@Autowired
	EmployeeDao dao;
	
	@Override
	public boolean insertEmp(Employee employee) {
		return dao.insertEmp(employee);
	}

	@Override
	public boolean deleteEmp(int id) {
		return dao.deleteEmp(id);
	}

	@Override
	public HashSet<String> getAllEmployee() {
		return dao.getAllEmployee();
	}

	@Override
	public boolean updateEmp(Employee employee) {
		return dao.updateEmp(employee);
	}

	@Override
	public Employee getEmp(int eId) {
		return dao.getEmp(eId);
	}

	@Override
	public Set<Integer> getNonBillExperinceList() {
		return dao.getNonBillExperinceList();
	}

	@Override
	public Map<Integer, Integer> getNonBillExpCount() {
		return dao.getNonBillExpCount();
	}

	@Override
	public Map<String, Integer> getStackCount() {
		return dao.getStackCount();
	}

	@Override
	public List<Object> getCountPaymentType() {
		return dao.getCountPaymentType();
	}

	@Override
	public List<Employee> getByTechno(String techno) {
		return dao.getByTechno(techno);
	}

	@Override
	public List<Employee> getAllEmps() {
		return dao.getAllEmps();
	}

	@Override
	public boolean updateOnMapProj(Employee employee,int proId) {
		return dao.updateOnMapProj(employee,proId);
	}

	@Override
	public List<ClientProject> getbyProjectId(int projId) {
		return dao.getbyProjectId(projId);
	}

}
