package com.tyss.tyoice.dto;

import lombok.Data;

@Data
public class ClientProjectResponse {

	private String message;

	private boolean error;

	private Object data;
}
