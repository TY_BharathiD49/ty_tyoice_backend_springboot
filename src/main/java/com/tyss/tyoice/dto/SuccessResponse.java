package com.tyss.tyoice.dto;

import lombok.Data;

@Data
public class SuccessResponse {

	private boolean error;

	private Object data;

}
