package com.tyss.tyoice.dto;

import lombok.Data;

@Data
public class ErrorResponse {

	private String message;
	
	private boolean error;
}
